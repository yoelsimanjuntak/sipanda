<?php
define('TBL__ROLES','_roles');
define('TBL__SETTINGS','_settings');
define('TBL__USERINFORMATION','_userinformation');
define('TBL__USERS','_users');
define('TBL_TMEETING','tmeeting');
define('TBL_TMEETINGLOGS','tmeetinglogs');
define('TBL_TMEETINGPARTICIPANTS','tmeetingparticipants');

define('COL_ROLEID','RoleID');
define('COL_ROLENAME','RoleName');
define('COL_SETTINGID','SettingID');
define('COL_SETTINGLABEL','SettingLabel');
define('COL_SETTINGNAME','SettingName');
define('COL_SETTINGVALUE','SettingValue');
define('COL_USERNAME','UserName');
define('COL_EMAIL','Email');
define('COL_NM_FIRSTNAME','NM_FirstName');
define('COL_NM_LASTNAME','NM_LastName');
define('COL_NM_NICKNAME','NM_Nickname');
define('COL_NM_GENDER','NM_Gender');
define('COL_DATE_BIRTH','DATE_Birth');
define('COL_NO_PHONE','NO_Phone');
define('COL_NM_ADDRESS','NM_Address');
define('COL_NM_PROVINCE','NM_Province');
define('COL_NM_CITY','NM_City');
define('COL_NM_REGION','NM_Region');
define('COL_NM_OCCUPATION','NM_Occupation');
define('COL_NM_PROFILEIMAGE','NM_ProfileImage');
define('COL_NUM_RATEREGULAR','NUM_RateRegular');
define('COL_NUM_RATEONLINE','NUM_RateOnline');
define('COL_NUM_EXPERIENCE','NUM_Experience');
define('COL_NM_BIO','NM_Bio');
define('COL_NM_MARITALSTATUS','NM_MaritalStatus');
define('COL_NM_EDUCATIONALBACKGROUND','NM_EducationalBackground');
define('COL_NM_GRADE','NM_Grade');
define('COL_DATE_REGISTERED','DATE_Registered');
define('COL_IS_EMAILVERIFIED','IS_EmailVerified');
define('COL_IS_LOGINVIAGOOGLE','IS_LoginViaGoogle');
define('COL_IS_LOGINVIAFACEBOOK','IS_LoginViaFacebook');
define('COL_NM_GOOGLEOAUTHID','NM_GoogleOAuthID');
define('COL_PASSWORD','Password');
define('COL_ISSUSPEND','IsSuspend');
define('COL_LASTLOGIN','LastLogin');
define('COL_LASTLOGINIP','LastLoginIP');
define('COL_IDMEETING','IdMeeting');
define('COL_NMJUDUL','NmJudul');
define('COL_NMPENYELENGGARA','NmPenyelenggara');
define('COL_NMLOKASI','NmLokasi');
define('COL_NMKETERANGAN','NmKeterangan');
define('COL_NMTERMS','NmTerms');
define('COL_NMVIA','NmVia');
define('COL_NMURL','NmURL');
define('COL_NMSUMMARY','NmSummary');
define('COL_NMFILE','NmFile');
define('COL_DATEJADWAL','DateJadwal');
define('COL_TIMEFROM','TimeFrom');
define('COL_TIMETO','TimeTo');
define('COL_ISBATAL','IsBatal');
define('COL_CREATEDBY','CreatedBy');
define('COL_CREATEDON','CreatedOn');
define('COL_UPDATEDBY','UpdatedBy');
define('COL_UPDATEDON','UpdatedOn');
define('COL_UNIQ','Uniq');
define('COL_IDPARTICIPANT','IdParticipant');
define('COL_TIMESTAMP','Timestamp');
define('COL_NMPESERTA','NmPeserta');
define('COL_NMEMAIL','NmEmail');
define('COL_NMPHONENO','NmPhoneNo');
define('COL_ISINVITATIONSENT','IsInvitationSent');
