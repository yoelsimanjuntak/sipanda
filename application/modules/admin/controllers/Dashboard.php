<?php
class Dashboard extends MY_Controller {
  function __construct() {
    parent::__construct();

    if(IsLogin()) {
      $ruser = GetLoggedUser();
      if($ruser[COL_ROLEID] != ROLEADMIN) {
        //redirect('site/home');
      }
    }
  }

  private function SendMail($to, $subj, $content) {
    $CI =& get_instance();
    $CI->load->library('Mailer');
    $mail = $CI->mailer->load();

    $mail->isSMTP();
    //$mail->SMTPDebug = 3;
    $mail->Debugoutput = 'html';
    $mail->Host     = SETTING_SMTPHOST;
    $mail->SMTPAuth = true;
    $mail->Username = SETTING_SMTPEMAIL;
    $mail->Password = SETTING_SMTPPASSWORD;
    $mail->SMTPSecure = 'ssl';
    $mail->Port     = SETTING_SMTPPORT;

    $mail->setFrom(SETTING_EMAILSENDER, SETTING_EMAILSENDERNAME);
    if(is_array($to)) {
      foreach ($to as $t) {
        $mail->addAddress($t);
      }
    } else {
      $mail->addAddress($to);
    }

    $mail->Subject = $subj;

    // Set email format to HTML
    $mail->isHTML(true);

    // Email body content
    $mailContent = $content;
    $mail->Body = $mailContent;

    // Send email
    if(!$mail->send()){
        return false;
    } else{
        return true;
    }
  }

  public function index() {
    if(!IsLogin()) {
      //redirect('site/home/login');
    }

    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID] != ROLEADMIN) {
      //redirect('site/home');
    }

    $data['title'] = "Dashboard";
    $this->template->load('main', 'admin/dashboard/index', $data);
  }

  public function data_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];

    $ruser = GetLoggedUser();
    $orderdef = array(COL_DATEJADWAL=>'desc');
    $orderables = array(null,null,COL_NMJUDUL,COL_NMPENYELENGGARA,null);
    $cols = array(COL_DATEJADWAL,COL_NMJUDUL,COL_NMPENYELENGGARA);

    $queryAll = $this->db
    ->get(TBL_TMEETING);

    $i = 0;
    foreach($cols as $item){
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
        $order = $orderdef;
        $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->select('*, (select count(*) from tmeetingparticipants p where p.IdMeeting = tmeeting.IdMeeting) as JlhPeserta')
    ->get_compiled_select(TBL_TMEETING, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $data[] = array(
        '<a href="'.site_url('admin/dashboard/data-delete/'.$r[COL_IDMEETING]).'" class="btn btn-xs btn-outline-danger btn-action" title="Hapus"><i class="fas fa-trash"></i></a>&nbsp;'.
        '<a href="'.site_url('admin/dashboard/data-view/'.$r[COL_IDMEETING]).'" class="btn btn-xs btn-outline-info btn-view" title="Lihat Detil"><i class="fas fa-info-circle"></i></a>&nbsp;'.
        '<a href="'.site_url('admin/dashboard/data-notes/'.$r[COL_IDMEETING]).'" class="btn btn-xs btn-outline-primary btn-notes" title="Notulen"><i class="fas fa-sticky-note"></i></a>&nbsp;'.
        '<a href="'.site_url('admin/dashboard/data-print/'.$r[COL_IDMEETING]).'" class="btn btn-xs btn-outline-success" title="Cetak Hasil" target="_blank"><i class="fas fa-print"></i></a>',
        date('Y-m-d', strtotime($r[COL_DATEJADWAL])),
        $r[COL_TIMEFROM].' s.d '.$r[COL_TIMETO],
        $r[COL_NMJUDUL],
        $r[COL_NMPENYELENGGARA],
        number_format($r['JlhPeserta'])
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function data_add() {
    $ruser = GetLoggedUser();
    $selkategori = '';
    if(!empty($_POST)) {
      $det = $this->input->post("Participants");

      $config['upload_path'] = MY_UPLOADPATH;
      $config['allowed_types'] = UPLOAD_ALLOWEDTYPES;
      $config['max_size'] = 102400;
      $config['max_width']  = 2560;
      $config['max_height']  = 2560;
      $config['overwrite'] = FALSE;

      $data = array(
        COL_NMJUDUL => $this->input->post(COL_NMJUDUL),
        COL_NMPENYELENGGARA => $this->input->post(COL_NMPENYELENGGARA),
        COL_NMURL => $this->input->post(COL_NMURL),
        COL_DATEJADWAL => $this->input->post(COL_DATEJADWAL),
        COL_TIMEFROM => $this->input->post(COL_TIMEFROM),
        COL_TIMETO => $this->input->post(COL_TIMETO),
        COL_NMKETERANGAN => $this->input->post(COL_NMKETERANGAN),
        COL_CREATEDBY => $ruser[COL_USERNAME],
        COL_CREATEDON => date('Y-m-d H:i:s')
      );

      if(!empty($_FILES["userfile"]["name"])) {
        $this->load->library('upload',$config);
        if(!$this->upload->do_upload("userfile")){
            $err = $this->upload->display_errors();
            ShowJsonError(strip_tags($err));
            return;
        }

        $dataupload = $this->upload->data();
        if(!empty($dataupload) && $dataupload['file_name']) {
            $data[COL_NMFILE] = $dataupload['file_name'];
        }
      }

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_TMEETING, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $idMeeting = $this->db->insert_id();
        if(!empty($det)) {
          $det = json_decode(urldecode($det));
          foreach($det as $s) {
            $arrDet[] = array(
              COL_IDMEETING=>$idMeeting,
              COL_NMPESERTA=>$s->NmPeserta,
              COL_NMEMAIL=>$s->NmEmail,
              COL_NMPHONENO=>$s->NmPhoneNo
            );
          }
        }

        if(!empty($arrDet)) {
          $res = $this->db->insert_batch(TBL_TMEETINGPARTICIPANTS, $arrDet);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }
        } else {
          throw new Exception('Peserta tidak boleh kosong.');
        }

        $this->db->trans_commit();
        ShowJsonSuccess('INPUT DATA BERHASIL.');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data = array('Participant'=>json_encode(array()));
      $this->load->view('admin/dashboard/form_data', $data);
    }
  }

  public function data_delete($id) {
    $res = $this->db->where(COL_IDMEETING, $id)->delete(TBL_TMEETING);
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      return;
    } else {
      ShowJsonSuccess('OK');
      return;
    }
  }

  public function data_view($id) {
    $rdata = $this->db
    ->select('tmeeting.*, uc.Nm_FirstName as Nm_CreatedBy')
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TMEETING.".".COL_CREATEDBY,"left")
    ->where(COL_IDMEETING, $id)
    ->get(TBL_TMEETING)
    ->row_array();
    if(empty($rdata)) {
      echo 'Data tidak ditemukan';
      return;
    }

    $arrdet = array();
    $rdet = $this->db
    ->select('tmeetingparticipants.*')
    ->where(COL_IDMEETING, $rdata[COL_IDMEETING])
    ->get(TBL_TMEETINGPARTICIPANTS)
    ->result_array();
    foreach($rdet as $det) {
      $arrdet[] = array(
        'NmPeserta'=> $det[COL_NMPESERTA],
        'NmEmail'=> $det[COL_NMEMAIL],
        'NmPhoneNo'=> $det[COL_NMPHONENO]
      );
    }
    $this->load->view('admin/dashboard/form_data', array('Participant'=>json_encode($arrdet),'data'=>$rdata,'disabled'=>true));
  }

  public function data_notes($id) {
    $rdata = $this->db
    ->select('tmeeting.*, uc.Nm_FirstName as Nm_CreatedBy')
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TMEETING.".".COL_CREATEDBY,"left")
    ->where(COL_IDMEETING, $id)
    ->get(TBL_TMEETING)
    ->row_array();
    if(empty($rdata)) {
      echo 'Data tidak ditemukan';
      return;
    }
    if(!empty($_POST)) {
      $notes = $this->input->post(COL_NMSUMMARY);
      $res = $this->db->where(COL_IDMEETING, $id)->update(TBL_TMEETING, array(COL_NMSUMMARY=>$notes));
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        return;
      }

      ShowJsonSuccess('RANGKUMAN BERHASIL DIINPUT');
      return;
    } else {
      $this->load->view('admin/dashboard/form_notes', array('data'=>$rdata));
    }
  }

  public function mail() {
    $this->load->view('admin/_layouts/mail-invite', array(
      'link'=>'_LINK_',
      'subj'=>'_SUBJ_',
      '_nama'=>'_NAMA_',
      '_judul'=>'_JUDUL_',
      '_host'=>'_HOST_',
      '_jadwal'=>'_JADWAL_',
    ));
  }

  public function send_invitation($id) {
    $rdata = $this->db
    ->where(COL_IDMEETING, $id)
    ->get(TBL_TMEETING)
    ->row_array();

    if(empty($rdata)) {
      ShowJsonError('DATA TIDAK DITEMUKAN');
      return;
    }

    $rparticipant = $this->db
    ->where(COL_IDMEETING, $id)
    ->get(TBL_TMEETINGPARTICIPANTS)
    ->result_array();

    foreach($rparticipant as $p) {
      $_nama = $p[COL_NMPESERTA];
      $_judul = $rdata[COL_NMJUDUL];
      $_host = $rdata[COL_NMPENYELENGGARA];
      $_jadwal = date('d/m/Y', strtotime($rdata[COL_DATEJADWAL]));

      $link = site_url('site/home/invitation/'.$p[COL_IDPARTICIPANT]);
      $mailSubj = 'UNDANGAN - '.$_judul;
      $mailContent = @"
      <p>
        Yth. $_nama,<br />Anda memiliki undangan pertemuan <strong>\"$_judul\"</strong> yang dilaksanakan pada <strong>$_jadwal</strong> oleh <strong>$_host</strong>.<br  />
        Silakan klik link <strong><a href=\"$link\">BERIKUT</a></strong> untuk melihat info detail dan melakukan absensi pada hari pelaksanaan.<br /><br />
        TERIMA KASIH.
      </p>
      $link
      ";
      $mailContent = $this->load->view('admin/_layouts/mail-invite', array(
        'link'=>$link,
        '_nama'=>$_nama,
        '_judul'=>$_judul,
        '_host'=>$_host,
        '_jadwal'=>$_jadwal)
      , TRUE);
      $send = SendMail($p[COL_NMEMAIL], $mailSubj, $mailContent);
      if($send) {
        $res = $this->db
        ->where(COL_IDPARTICIPANT, $p[COL_IDPARTICIPANT])
        ->update(TBL_TMEETINGPARTICIPANTS, array(COL_ISINVITATIONSENT=>1));
      }
    }

    ShowJsonSuccess('UNDANGAN TERIKIRIM.');
    return;
  }

  public function data_print($id) {
    $rdata = $this->db
    ->select('tmeeting.*, uc.Nm_FirstName as Nm_CreatedBy')
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TMEETING.".".COL_CREATEDBY,"left")
    ->where(COL_IDMEETING, $id)
    ->get(TBL_TMEETING)
    ->row_array();
    if(empty($rdata)) {
      echo 'Data tidak ditemukan';
      return;
    }

    $rdet = $this->db
    ->join(TBL_TMEETINGLOGS,TBL_TMEETINGLOGS.'.'.COL_IDPARTICIPANT." = ".TBL_TMEETINGPARTICIPANTS.".".COL_IDPARTICIPANT,"left")
    ->where(COL_IDMEETING, $rdata[COL_IDMEETING])
    ->get(TBL_TMEETINGPARTICIPANTS)
    ->result_array();

    $this->load->library('Mypdf');
    $mpdf = new Mypdf();

    $html = $this->load->view('admin/dashboard/form_print', array('data'=>$rdata, 'det'=>$rdet), TRUE);
    //echo $html;
    //return;
    $mpdf->pdf->SetTitle('Hasil Pertemuan - '.$rdata[COL_NMJUDUL]);
    $mpdf->pdf->SetFooter('Dicetak melalui aplikasi '.$this->setting_web_name.' - '.$this->setting_web_desc);
    $mpdf->pdf->WriteHTML($html);
    $mpdf->pdf->Output('Hasil Pertemuan - '.$rdata[COL_NMJUDUL].'.pdf', 'I');
  }
}
