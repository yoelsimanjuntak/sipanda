<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 font-weight-light"><?= $title ?></h3>
      </div><!-- /.col -->
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <?=form_open_multipart(current_url(),array('role'=>'form','id'=>'form-main','class'=>'form-horizontal'))?>
    <div class="row">
      <div class="col-sm-12">
        <div class="form-group">
          <a href="<?=site_url('admin/user/index')?>" class="btn btn-sm btn-secondary"><i class="far fa-arrow-left"></i>&nbsp;KEMBALI</a>
          <button type="submit" class="btn btn-sm btn-primary"><i class="far fa-save"></i>&nbsp;SIMPAN</button>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="card card-outline card-olive">
          <div class="card-header">
            <h5 class="card-title font-weight-light">Akun & Data Diri</h5>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-sm-12">
                <?php
                if(empty($role) || true) {
                  ?>
                  <div class="form-group">
                    <label>Kategori</label>
                    <select class="form-control" name="<?=COL_ROLEID?>" required>
                      <?=GetCombobox("SELECT * FROM mkategori ORDER BY NmKategori", COL_IDKATEGORI, COL_NMKATEGORI, (!empty($data)?$data[COL_ROLEID]:null))?>
                    </select>
                  </div>
                  <?php
                }
                ?>
                <div class="form-group">
                    <div class="row">
                      <div class="col-sm-6">
                        <label>Email / Username</label>
                          <input type="text" class="form-control" name="<?=COL_EMAIL?>" placeholder="Email" value="<?=!empty($data)?$data[COL_EMAIL]:''?>" <?=!empty($data)?'readonly':''?> />
                      </div>
                      <div class="col-sm-6">
                        <label>Nama</label>
                        <input type="text" class="form-control" name="<?=COL_NM_FIRSTNAME?>" placeholder="Nama Lengkap" value="<?=!empty($data)?$data[COL_NM_FIRSTNAME]:''?>" required />
                      </div>
                    </div>
                </div>
                <div class="form-group row">
                  <div class="col-sm-6">
                    <label>Tanggal Lahir</label>
                    <input type="text" class="form-control datepicker" name="<?=COL_DATE_BIRTH?>" placeholder="yyyy-mm-dd" value="<?=!empty($data)?$data[COL_DATE_BIRTH]:''?>" required />
                  </div>
                  <div class="col-sm-6">
                    <label>No. Telp / HP</label>
                    <input type="text" class="form-control" name="<?=COL_NO_PHONE?>" placeholder="No. Telp / HP" value="<?=!empty($data)?$data[COL_NO_PHONE]:''?>" required />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <?php
        if(empty($data)) {
          ?>
          <div class="card card-outline card-info">
            <div class="card-header">
              <h5 class="card-title font-weight-light">Password</h5>
            </div>
            <div class="card-body">
              <div class="form-group row">
                <label class="control-label col-sm-4">Password</label>
                <div class="col-sm-8">
                  <input type="password" class="form-control" name="<?=COL_PASSWORD?>" placeholder="Password" />
                </div>
              </div>
              <div class="form-group row">
                <label class="control-label col-sm-4">Konfirmasi Password</label>
                <div class="col-sm-8">
                  <input type="password" class="form-control" name="ConfirmPassword" placeholder="Konfirmasi Password" />
                </div>
              </div>
            </div>
          </div>
          <?php
        }
        ?>
      </div>
    </div>
    <?=form_close()?>
  </div>
</section>
<script>
$(document).ready(function() {
  $('.calendar').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minDate: moment().format('YYYY-MM-DD'),
    maxYear: parseInt(moment().format('YYYY'),10),
    locale: {
        format: 'Y-MM-DD'
    }
  });

  $('button[data-verify=true]').click(function() {
    $('[name=Verify]').val(1);
    $('#form-main').submit();
  });
  $('#form-main').validate({
    ignore: "[type=file]",
    submitHandler: function(form) {
      if(!confirm('Apakah anda yakin?')) {
        return false;
      }

      var btnSubmit = $('button[type=submit], button[data-verify=true]', $(form));
      btnSubmit.attr('disabled', true);
      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success('Berhasil');
            if(res.data && res.data.redirect) {
              setTimeout(function(){
                location.href = res.data.redirect;
              }, 1000);
            }
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.attr('disabled', false);
        }
      });
      return false;
    }
  });
});
</script>
