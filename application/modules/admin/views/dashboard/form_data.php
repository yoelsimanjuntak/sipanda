<style>
#tbl-participant td, #tbl-participant th {
	padding: .5rem;
}
</style>
<?=form_open_multipart(current_url(), array('id'=>'form-data', 'class'=>'form-horizontal'))?>
<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
			<label class="text-sm">JUDUL</label>
			<input type="textbox" class="form-control form-control-sm" name="<?=COL_NMJUDUL?>" value="<?=!empty($data)?$data[COL_NMJUDUL]:''?>" />
		</div>
		<div class="form-group">
			<label class="text-sm">PENYELENGGARA</label>
			<input type="textbox" class="form-control form-control-sm" name="<?=COL_NMPENYELENGGARA?>" value="<?=!empty($data)?$data[COL_NMPENYELENGGARA]:''?>" />
		</div>
		<div class="form-group">
		  <label class="text-sm">LAMPIRAN</label><br />
			<?php
			if(!empty($data)) {
				if(!empty($data[COL_NMFILE])) {
					?>
					<a href="<?=MY_UPLOADURL.$data[COL_NMFILE]?>"  target="_blank"><?=$data[COL_NMFILE]?></a>
					<?php
				} else {
					?>
					--
					<?php
				}
			}
			else {
				?>
				<input type="file" name="userfile" />
				<?php
			}
			?>
		</div>
	</div>
	<div class="col-sm-6">
		<div class="form-group row">
			<div class="col-sm-6">
				<label class="text-sm">TANGGAL</label>
				<input type="textbox" class="form-control form-control-sm datepicker" name="<?=COL_DATEJADWAL?>" value="<?=!empty($data)?$data[COL_DATEJADWAL]:''?>" />
			</div>
			<div class="col-sm-3">
				<label class="text-sm">MULAI</label>
				<input type="textbox" class="form-control form-control-sm <?=isset($disabled)&&$disabled?'':'timepicker'?>" placeholder="hh:mm" name="<?=COL_TIMEFROM?>" value="<?=!empty($data)?$data[COL_TIMEFROM]:''?>" />
			</div>
			<div class="col-sm-3">
				<label class="text-sm">SELESAI</label>
					<input type="textbox" class="form-control form-control-sm <?=isset($disabled)&&$disabled?'':'timepicker'?>" placeholder="hh:mm" name="<?=COL_TIMETO?>" value="<?=!empty($data)?$data[COL_TIMETO]:''?>" />
			</div>
		</div>
		<div class="form-group">
			<label class="text-sm">URL</label>
			<input type="textbox" class="form-control form-control-sm" name="<?=COL_NMURL?>" value="<?=!empty($data)?$data[COL_NMURL]:''?>" />
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="form-group">
			<label class="text-sm">KETERANGAN</label>
			<?php
			if(isset($disabled) && $disabled) {
				?>
				<div><?=$data[COL_NMKETERANGAN]?></div>
				<?php
			} else {
				?>
				<textarea rows="5" id="remarks" name="<?=COL_NMKETERANGAN?>" class="form-control form-control-sm"><?=!empty($data)?$data[COL_NMKETERANGAN]:''?></textarea>
				<?php
			}
			?>
		</div>
	</div>
</div>
	
<div class="row">
	<div class="col-sm-12">
        <div class="form-group pt-2" style="border-top: 1px solid #dedede">
          <input type="hidden" name="Participants" />
          <div class="row">
            <div class="col-sm-12">
							<?php
							if(isset($disabled) && $disabled && !empty($data)) {
								?>
								<p class="mt-2 mb-1 text-center">
									<button data-href="<?=site_url('admin/dashboard/send-invitation/'.$data[COL_IDMEETING])?>" class="btn btn-sm btn-info btn-send"><i class="far fa-paper-plane"></i>&nbsp;KIRIM UNDANGAN</button>
								</p>
								<?php
							}
							?>
              <table id="tbl-participant" class="table table-bordered">
                <thead class="bg-gray">
                  <tr>
                    <th class="text-sm">Nama</th>
                    <th class="text-sm">Email</th>
                    <th class="text-sm">No. HP</th>
										<?php
										if(!isset($disabled)) {
											?>
											<th class="text-center">#</th>
											<?php
										}
										?>
                  </tr>
                </thead>
                <tbody class="text-sm">

                </tbody>
								<?php
								if(!isset($disabled)) {
									?>
									<tfoot>
	                  <tr>
	                    <th style="width: 300px">
	                      <input type="text" class="form-control form-control-sm" name="<?=COL_NMPESERTA?>" placeholder="Nama Peserta" style="width: 100%" />
	                    </th>
	                    <th>
	                      <input type="text" class="form-control form-control-sm" name="<?=COL_NMEMAIL?>" placeholder="Email" />
	                    </th>
	                    <th>
	                      <input type="text" class="form-control form-control-sm" name="<?=COL_NMPHONENO?>" placeholder="No. HP" />
	                    </th>
	                    <th style="white-space: nowrap;" class="text-center">
	                      <button type="button" id="btn-add-participant" class="btn btn-xs btn-default"><i class="fa fa-plus"></i></button>
	                    </th>
	                  </tr>
	                </tfoot>
									<?php
								}
								?>
              </table>
            </div>
          </div>
        </div>
    </div>
</div>
 <?php
 if(!empty($data)) {
 	?>
 	<p class="text-muted text-sm font-italic label-info mb-0">
 		Diinput oleh: <b><?=$data['Nm_CreatedBy']?></b><br />
 		Diinput pada: <b><?=date('Y-m-d H:i:s', strtotime($data[COL_CREATEDON]))?></b>
	</p>
 	<?php
 }
 ?>
 <?php
 if(!isset($disabled) || !$disabled) {
	 ?>
	 <div class="form-group row">
	 	<div class="col-sm-12 text-right">
	 		<button type="button" class="btn btn-outline-danger" data-dismiss="modal"><i class="far fa-times"></i>&nbsp;BATAL</button>
	 		<button type="submit" class="btn btn-outline-success"><i class="far fa-save"></i>&nbsp;SIMPAN</button>
	 	</div>
	 </div>
	 <?php
 }
 ?>
<?=form_close()?>
<script>
$(document).ready(function() {
  var form = $('#form-data');
  $('.datepicker', form).daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minYear: parseInt(moment().format('YYYY'),10),
    maxYear: parseInt(moment().format('YYYY'),10),
    locale: {
        format: 'Y-MM-DD'
    }
  });
	$('.timepicker').timepicker({
		timeFormat: 'HH:mm',
		interval: 30,
		minTime: '7',
		maxTime: '21',
		defaultTime: '7',
		startTime: '07:00',
		dynamic: false,
		dropdown: true,
		scrollbar: true,
		zindex: 9999999
	});

	<?php
	if(!isset($disabled) || !$disabled) {
		?>
		CKEDITOR.config.height = 200;
		CKEDITOR.replace("remarks", {
			toolbar: [
				{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'CopyFormatting', 'RemoveFormat' ] },
				{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
				{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
				{ name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak' ] },
				{ name: 'colors', items: [ 'TextColor', 'BGColor' ] },
				{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] }
			]
		});
		<?php
	}
	?>
  $('[name=Participants]', form).change(function() {
    writeParticipant('tbl-participant', 'Participants');
  }).val(encodeURIComponent('<?=$Participant?>')).trigger('change');

  $('#btn-add-participant', $('#tbl-participant')).click(function() {
    var dis = $(this);
    var arr = $('[name=Participants]').val();
    if(arr) arr = JSON.parse(decodeURIComponent(arr));
    else arr = [];

    var row = dis.closest('tr');
    var nama = $('[name=NmPeserta]', row).val();
    var email = $('[name=NmEmail]', row).val();
    var phoneno = $('[name=NmPhoneNo]', row).val();
    if(nama) {
      var exist = jQuery.grep(arr, function(a) {
        return a.NmPeserta == nama;
      });
      if(exist.length == 0) {
        arr.push({'NmPeserta':nama, 'NmEmail':email, 'NmPhoneNo':phoneno});
        $('[name=Participants]').val(encodeURIComponent(JSON.stringify(arr))).trigger('change');
        $('input', row).val('');
      } else {
        alert('Nama tersebut sudah ada.');
        return false;
      }
    } else {
      alert('Harap isi kolom isian dengan benar.');
      return false;
    }
  });
  <?php
  if(!empty($disabled)) {
    ?>
    $('input,select,textarea,button', form).not('[data-dismiss=modal],.btn-send').attr('disabled', true);
    <?php
  }
  ?>

	$('.btn-send', form).click(function(){
		var dis = $(this);
		var ajax = dis.data('href');
		var html = dis.html();

		dis.attr('disabled', true);
		dis.html('MENGIRIM...');
		$.get(ajax, function(res) {
			if(res.error != 0) {
				toastr.error(res.error);
			} else {
				toastr.success(res.success);
			}
		}, "json").done(function() {
			dis.attr('disabled', false);
			dis.html(html);
		}).fail(function() {
			toastr.error('SERVER ERROR');
			dis.attr('disabled', false);
			dis.html(html);
		});
		return false;
	});
});

function writeParticipant(tbl, input) {
  var tbl = $('#'+tbl+'>tbody');
  var arr = $('[name='+input+']').val();
  if(arr) {
    arr = JSON.parse(decodeURIComponent(arr));
    if(arr.length > 0) {
      var html = '';
      for (var i=0; i<arr.length; i++) {
        html += '<tr>';
        html += '<td>'+arr[i].NmPeserta+'</td>';
        html += '<td>'+(arr[i].NmEmail?arr[i].NmEmail:'(kosong)')+'</td>';
        html += '<td>'+(arr[i].NmPhoneNo?arr[i].NmPhoneNo:'(kosong)')+'</td>';
				<?php
				if(!isset($disabled)) {
					?>
					html += '<td class="text-center"><button type="button" class="btn btn-xs btn-outline-danger btn-del"><i class="fas fa-minus"></i></button><input type="hidden" name="idx" value="'+arr[i].NmPeserta+'" /></td>';
					<?php
				}
				?>
        html += '</tr>';
      }
      tbl.html(html);

      $('.btn-del', tbl).click(function() {
        var row = $(this).closest('tr');
        var idx = $('[name=idx]', row).val();
        if(idx) {
          var arr = $('[name='+input+']').val();
          arr = JSON.parse(decodeURIComponent(arr));

          var arrNew = $.grep(arr, function(e){ return e.NmPeserta != idx; });
          $('[name='+input+']').val(encodeURIComponent(JSON.stringify(arrNew))).trigger('change');
        }
      });
    } else {
      tbl.html('<tr><td colspan="5"><p class="font-italic text-center m-0">Belum ada data</p></td></tr>');
    }
  } else {
    tbl.html('<tr><td colspan="5"><p class="font-italic text-center m-0">Belum ada data</p></td></tr>');
  }
}
</script>
