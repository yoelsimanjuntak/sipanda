<?php
$ruser = GetLoggedUser();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 font-weight-light"><?= $title ?></h3>
      </div><!-- /.col -->
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-outline card-info">
          <div class="card-header">
            <h5 class="card-title font-weight-light">PERTEMUAN</h5>
            <div class="card-tools">
              <?php
              if($ruser[COL_ROLEID] == ROLEADMIN) {
                ?>
                <a href="<?=site_url('admin/dashboard/data-add')?>" type="button" class="btn btn-tool btn-add-data text-info"><i class="fas fa-plus"></i>&nbsp;TAMBAH</a>
                <?php
              }
              ?>
              <button type="button" class="btn btn-tool btn-refresh-data"><i class="fas fa-sync-alt"></i>&nbsp;REFRESH</button>
            </div>
          </div>
          <div class="card-body">
            <form method="post" action="#">
              <table id="list-data" class="table table-bordered" style="white-space: nowrap;">
                <thead>
                  <tr>
                    <th class="text-center" style="width: 10px">AKSI</th>
                    <th>Tanggal</th>
                    <th>Jam</th>
                    <th>Judul</th>
                    <th>Penyelenggara</th>
                    <th>Jlh. Peserta</th>
                  </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-data" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header pb-1">
        <h4 class="modal-title font-weight-light">Form</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fas fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body">

      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function() {
  var modalData = $('#modal-data');

  var dt = $('#list-data').dataTable({
    "autoWidth" : false,
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?=site_url('admin/dashboard/data-load')?>",
      "type": 'POST',
    },
    "scrollY" : '30vh',
    "scrollX": "200%",
    "iDisplayLength": 100,
    //"aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
    //"dom":"R<'row'<'col-sm-8'l><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
    "dom":"R<'row'<'col-sm-6'l><'col-sm-6'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
    "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
    "order": [],
    "columnDefs": [{"targets":[0], "className":'text-center'}, {"targets":[1,2], "className":'dt-body-right'}, {"targets":[5], "className":'dt-body-center'}],
    "columns": [
      {"orderable": false,"width": "10px"},
      {"orderable": true,"width": "100px"},
      {"orderable": true,"width": "100px"},
      {"orderable": true},
      {"orderable": true},
      {"orderable": false,"width": "100px"}
    ],
    "createdRow": function(row, data, dataIndex) {
      $('.btn-action', $(row)).click(function() {
        var url = $(this).attr('href');
        if(confirm('Apakah anda yakin?')) {
          $.get(url, function(res) {
            if(res.error != 0) {
              toastr.error(res.error);
            } else {
              toastr.success(res.success);
            }
          }, "json").done(function() {
            dt.DataTable().ajax.reload();
          }).fail(function() {
            toastr.error('SERVER ERROR');
          });
        }
        return false;
      });
      $('.btn-view', $(row)).click(function() {
        var url = $(this).attr('href');
        $('.modal-body', modalData).load(url, function() {
          modalData.modal('show');
        });
        return false;
      });
      $('.btn-notes', $(row)).click(function() {
        var url = $(this).attr('href');
        $('.modal-body', modalData).load(url, function() {
          modalData.modal('show');
          $('form', modalData).validate({
            submitHandler: function(form) {
              var btnSubmit = $('button[type=submit]', $('form', modalData));
              var txtSubmit = btnSubmit[0].innerHTML;
              btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
              $('form', modalData).ajaxSubmit({
                dataType: 'json',
                type : 'post',
                success: function(res) {
                  if(res.error != 0) {
                    toastr.error(res.error);
                  } else {
                    toastr.success(res.success);
                    modalData.modal('hide');
                  }
                },
                error: function() {
                  toastr.error('SERVER ERROR');
                },
                complete: function() {
                  btnSubmit.html(txtSubmit);
                }
              });
              return false;
            }
          });
        });
        return false;
      });
    }
  });

  $('.modal').on('hidden.bs.modal', function (event) {
      $('form', $(this)).val('').trigger('change');
  });

  $('.btn-refresh-data').click(function() {
    dt.DataTable().ajax.reload();
  });

  $('.btn-add-data').click(function() {
    var href = $(this).attr('href');
    $('.modal-body', modalData).load(href, function() {
      modalData.modal('show');
      $('.datepicker', modalData).daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        minYear: 1990,
        maxYear: parseInt(moment().format('YYYY'),10),
        locale: {
          format: 'Y-MM-DD'
        }
      });
      $("select.tag-input", modalData).select2({ width: 'resolve', theme: 'bootstrap4' });

      $('form', modalData).validate({
        submitHandler: function(form) {
          var btnSubmit = $('button[type=submit]', $(form));
          var txtSubmit = btnSubmit[0].innerHTML;
          btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
          $(form).ajaxSubmit({
            dataType: 'json',
            type : 'post',
            success: function(res) {
              if(res.error != 0) {
                toastr.error(res.error);
              } else {
                toastr.success(res.success);
                dt.DataTable().ajax.reload();
                modalData.modal('hide');
              }
            },
            error: function() {
              toastr.error('SERVER ERROR');
            },
            complete: function() {
              btnSubmit.html(txtSubmit);
            }
          });
          return false;
        }
      });
    });
    return false;
  });
});
</script>
