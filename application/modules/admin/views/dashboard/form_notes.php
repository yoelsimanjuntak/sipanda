<?=form_open_multipart(current_url(), array('id'=>'form-notes', 'class'=>'form-horizontal'))?>
<table class="table table-striped">
  <tbody>
    <tr>
      <td style="width: 100px">JUDUL</td>
      <td style="width: 10px">:</td>
      <td class="font-weight-bold"><?=$data[COL_NMJUDUL]?></td>
    </tr>
    <tr>
      <td style="width: 100px">PENYELENGGARA</td>
      <td style="width: 10px">:</td>
      <td class="font-weight-bold"><?=$data[COL_NMPENYELENGGARA]?></td>
    </tr>
    <tr>
      <td style="width: 100px">TANGGAL</td>
      <td style="width: 10px">:</td>
      <td class="font-weight-bold"><?=$data[COL_DATEJADWAL]?></td>
    </tr>
    <tr>
      <td style="width: 100px">JAM</td>
      <td style="width: 10px">:</td>
      <td class="font-weight-bold"><?=$data[COL_TIMEFROM].' s.d '.$data[COL_TIMETO]?></td>
    </tr>
  </tbody>
</table>
<div class="form-group">
  <label>RANGKUMAN:</label>
  <textarea id="notes" rows="4" class="form-control" placeholder="Rangkuman / Intisari / Notulen" name="<?=COL_NMSUMMARY?>"><?=!empty($data)?$data[COL_NMSUMMARY]:''?></textarea>
</div>
<div class="form-group row">
 <div class="col-sm-12 text-right">
   <button type="button" class="btn btn-outline-danger" data-dismiss="modal"><i class="far fa-times"></i>&nbsp;BATAL</button>
   <button type="submit" class="btn btn-outline-success"><i class="far fa-save"></i>&nbsp;SIMPAN</button>
 </div>
</div>
<?=form_close()?>
<script>
  CKEDITOR.config.height = 200;
    CKEDITOR.replace("notes", {
      toolbar: [
        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'CopyFormatting', 'RemoveFormat' ] },
        { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
        { name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
        { name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
        { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
        { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] }
      ]
    });
</script>