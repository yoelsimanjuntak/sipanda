<html>
<head>
  <title>Hasil Pertemuan - <?=$data[COL_NMJUDUL]?></title>
  <style>
  th, td {
    padding: 5px;
  }
  </style>
</head>
<body>
<table width="100%">
  <tr>
    <td colspan="2" style="text-align: center; vertical-align: middle">
      <h4 style="text-decoration: underline">LAPORAN HASIL PERTEMUAN</h4>
    </td>
  </tr>
</table>
<br />
<table width="100%" border="0" style="border: 1px solid #000000">
  <tr>
    <td style="vertical-align: top; width: 150px; white-space: nowrap">Judul</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td style="vertical-align: top; font-weight: bold"><?=$data[COL_NMJUDUL]?></td>
  </tr>
  <tr>
    <td style="vertical-align: top; width: 150px; white-space: nowrap">Penyelenggara</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td style="vertical-align: top; font-weight: bold"><?=$data[COL_NMPENYELENGGARA]?></td>
  </tr>
  <tr>
    <td style="vertical-align: top; width: 150px; white-space: nowrap">Tanggal</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td style="vertical-align: top; font-weight: bold"><?=date('d/m/Y', strtotime($data[COL_DATEJADWAL]))?></td>
  </tr>
  <tr>
    <td style="vertical-align: top; width: 150px; white-space: nowrap">Waktu</td>
    <td style="vertical-align: top; width: 10px">:</td>
    <td style="vertical-align: top;"><strong><?=$data[COL_TIMEFROM]?></strong> s.d <strong><?=$data[COL_TIMETO]?></strong></td>
  </tr>
</table>
<br />
<h4>1. DESKRIPSI</h4>
<?=!empty($data[COL_NMKETERANGAN])?$data[COL_NMKETERANGAN]:'<p style="font-style: italic; margin-top: 0">(kosong)</p>'?>
<h4>2. NOTULEN / RANGKUMAN</h4>
<?=!empty($data[COL_NMSUMMARY])?$data[COL_NMSUMMARY]:'<p style="font-style: italic; margin-top: 0">(kosong)</p>'?>
<pagebreak></pagebreak>
<h4>3. DAFTAR HADIR PESERTA</h4>
<table width="100%" border="1" style="border-spacing: 0 !important; ">
  <tr style="background-color: #ddd;">
    <td style="vertical-align: top; border: 1px solid #000000; text-align: center; font-weight: bold; width: 20px; white-space: nowrap">NO.</td>
    <td style="vertical-align: top; border: 1px solid #000000; text-align: center; font-weight: bold;">NAMA</td>
    <td style="vertical-align: top; border: 1px solid #000000; text-align: center; font-weight: bold; width: 100px;">HADIR</td>
  </tr>
  <?php
  $no = 1;
  foreach($det as $d) {
    ?>
    <tr>
      <td style="vertical-align: top; border: 1px solid #000000; text-align: center; width: 20px; white-space: nowrap"><?=$no?></td>
      <td style="vertical-align: top; border: 1px solid #000000;"><?=$d[COL_NMPESERTA]?></td>
      <td style="vertical-align: top; border: 1px solid #000000; width: 100px;"><?=!empty($d[COL_TIMESTAMP])?'Ya':'Tidak'?></td>
    </tr>
    <?php
    $no++;
  }
  ?>
</table>
