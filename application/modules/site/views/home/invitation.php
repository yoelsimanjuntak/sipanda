<?php
$ruser = GetLoggedUser();
$displayname = $ruser ? $ruser[COL_NM_FIRSTNAME] : "Guest";
$displaypicture = MY_IMAGEURL.'user.jpg';
if($ruser) {
    $displaypicture = $ruser[COL_NM_PROFILEIMAGE] ? MY_UPLOADURL.$ruser[COL_NM_PROFILEIMAGE] : MY_IMAGEURL.'user.jpg';
}
$arrmime_img = [
	'image/jpg',
	'image/jpeg',
	'image/png',
	'iamge/gif'
];
?>
<style>
.list-group-item:last-child {
  border-bottom: none;
}
</style>
<div class="content-wrapper">
  <section class="content pt-3">
    <div class="container">
      <div class="row">
        <div class="col-sm-8">
            <div class="card card-info card-outline">
            	<div class="card-header">
            		<h4 class="card-title"><?=$data[COL_NMJUDUL]?></h4>
            	</div>
            	<div class="card-body p-0">
            		<table class="table table-striped">
					  <tbody>
					  	<?php
					  	if(!empty($data[COL_NMFILE]) && in_array(mime_content_type(MY_UPLOADPATH.$data[COL_NMFILE]), $arrmime_img)) {
					  		?>
					  		<tr>
						      <td colspan="3">
						      	<img src="<?=MY_UPLOADURL.$data[COL_NMFILE]?>" style="max-width: 100%">
						      </td>
						    </tr>
					  		<?php
					  	}
					  	?>
					    <tr>
					      <td style="width: 100px">PENYELENGGARA</td>
					      <td style="width: 10px">:</td>
					      <td class="font-weight-bold"><?=$data[COL_NMPENYELENGGARA]?></td>
					    </tr>
					    <tr>
					      <td style="width: 100px">TANGGAL</td>
					      <td style="width: 10px">:</td>
					      <td class="font-weight-bold"><?=$data[COL_DATEJADWAL]?></td>
					    </tr>
					    <tr>
					      <td style="width: 100px">JAM</td>
					      <td style="width: 10px">:</td>
					      <td class="font-weight-bold"><?=$data[COL_TIMEFROM].' s.d '.$data[COL_TIMETO]?></td>
					    </tr>
					    <tr>
					      <td style="width: 100px">URL / LINK</td>
					      <td style="width: 10px">:</td>
					      <td class="font-weight-bold">
					      	<a href="<?=$data[COL_NMURL]?>" target="_blank" class="btn btn-sm btn-success"><i class="far fa-desktop-alt"></i>&nbsp;&nbsp;URL VIDEO CONFERENCE <small class="font-italic">(KLIK DISINI)</small></a>
					      </td>
					    </tr>
					    <?php
					  	if(!empty($data[COL_NMFILE]) && !in_array(mime_content_type(MY_UPLOADPATH.$data[COL_NMFILE]), $arrmime_img)) {
					  		?>
					  		<tr>
						      <td style="width: 100px">LAMPIRAN</td>
						      <td style="width: 10px">:</td>
						      <td class="font-weight-bold"><a href="<?=MY_UPLOADURL.$data[COL_NMFILE]?>" target="_blank"><?=$data[COL_NMFILE]?></a></td>
						    </tr>
					  		<?php
					  	}
					  	?>
					    <tr>
					  		<td colspan="3">
					  			<p><?=$data[COL_NMKETERANGAN]?></p>
					  		</td>
					  	</tr>
					  </tbody>
					</table>
            	</div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="card card-info card-outline">
            	<div class="card-header">
            		<h4 class="card-title">ABSENSI</h4>
            	</div>
            	<div class="card-body p-0">
            		<table class="table table-striped">
					  <tbody>
					    <tr>
					      <td style="width: 100px">NAMA</td>
					      <td style="width: 10px">:</td>
					      <td class="font-weight-bold"><?=$data[COL_NMPESERTA]?></td>
					    </tr>
					    <tr>
					      <td style="width: 100px">EMAIL</td>
					      <td style="width: 10px">:</td>
					      <td class="font-weight-bold"><?=$data[COL_NMEMAIL]?></td>
					    </tr>
					  </tbody>
					</table>
            	</div>
            	<div class="card-footer">
            		<a href="<?=site_url('site/home/check-in/'.$data[COL_IDPARTICIPANT])?>" class="btn btn-block btn-outline-info btn-hadir"><i class="fad fa-calendar-check"></i>&nbsp;<strong>HADIR</strong></a>
            	</div>
            </div>
        </div>
      </div>
    </div>
  </section>
</div>
<script>
	$(document).ready(function() {
		$('.btn-hadir').click(function(){
			var dis = $(this);
			var ajax = dis.attr('href');
			var html = dis.html();

			dis.attr('disabled', true);
			dis.html('MENGIRIM...');
			$.get(ajax, function(res) {
				if(res.error != 0) {
					toastr.error(res.error);
				} else {
					toastr.success(res.success);
				}
			}, "json").done(function() {
				dis.attr('disabled', false);
				dis.html(html);
			}).fail(function() {
				toastr.error('SERVER ERROR');
				dis.attr('disabled', false);
				dis.html(html);
			});
			return false;
		});
	});
</script>
