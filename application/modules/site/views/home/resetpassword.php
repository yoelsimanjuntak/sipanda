<div class="content-wrapper">
  <div class="login-box">
    <div class="login-logo">
      <a href="<?=site_url()?>">
        <img src="<?=MY_IMAGEURL.'logo.png'?>" height="60px" />
      </a>
    </div>
    <div class="card">
      <div class="card-body login-card-body">
        <?=form_open(current_url(),array('role'=>'form','id'=>'form-resetpassword','class'=>'form-horizontal'))?>
        <div class="row">
          <div class="col-sm-12">
            <p class="font-weight-light">Silakan isi password baru anda.</p>
            <div class="form-group row">
              <div class="input-group">
                <input type="password" name="<?=COL_PASSWORD?>" class="form-control" placeholder="Password baru" required />
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fas fa-key"></span>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group row">
              <div class="input-group">
                <input type="password" name="ConfirmPassword" class="form-control" placeholder="Ulangi Password Baru" required />
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fas fa-key"></span>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group row">
              <button type="submit" class="btn btn-block btn-outline-pallete-1 mb-1"><i class="fad fa-lock"></i>&nbsp;&nbsp;RESET PASSWORD</button>
            </div>
          </div>
        </div>
        <?=form_close()?>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function() {
  $('#form-resetpassword').validate({
    submitHandler: function(form) {
      var btnSubmit = $('button[type=submit]', $(form));
      var txtSubmit = btnSubmit[0].innerHTML;
      btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            if(res.data && res.data.redirect) {
              setTimeout(function(){
                location.href = res.data.redirect;
              }, 2000);
            }
            $('input', form).val('');
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
        }
      });
      return false;
    }
  });
});
</script>
