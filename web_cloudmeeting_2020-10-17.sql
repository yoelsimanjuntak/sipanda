# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.36-MariaDB)
# Database: web_cloudmeeting
# Generation Time: 2020-10-17 12:32:48 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table _roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_roles`;

CREATE TABLE `_roles` (
  `RoleID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `RoleName` varchar(50) NOT NULL,
  PRIMARY KEY (`RoleID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_roles` WRITE;
/*!40000 ALTER TABLE `_roles` DISABLE KEYS */;

INSERT INTO `_roles` (`RoleID`, `RoleName`)
VALUES
	(999,'Administrator');

/*!40000 ALTER TABLE `_roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_settings`;

CREATE TABLE `_settings` (
  `SettingID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `SettingLabel` varchar(50) NOT NULL,
  `SettingName` varchar(50) NOT NULL,
  `SettingValue` text NOT NULL,
  PRIMARY KEY (`SettingID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_settings` WRITE;
/*!40000 ALTER TABLE `_settings` DISABLE KEYS */;

INSERT INTO `_settings` (`SettingID`, `SettingLabel`, `SettingName`, `SettingValue`)
VALUES
	(1,'SETTING_WEB_NAME','SETTING_WEB_NAME','SIPANDA'),
	(2,'SETTING_WEB_DESC','SETTING_WEB_DESC','Sistem Informasi Pertemuan Daring'),
	(3,'SETTING_WEB_DISQUS_URL','SETTING_WEB_DISQUS_URL','https://general-9.disqus.com/embed.js'),
	(4,'SETTING_ORG_NAME','SETTING_ORG_NAME','-'),
	(5,'SETTING_ORG_ADDRESS','SETTING_ORG_ADDRESS','-'),
	(6,'SETTING_ORG_LAT','SETTING_ORG_LAT',''),
	(7,'SETTING_ORG_LONG','SETTING_ORG_LONG',''),
	(8,'SETTING_ORG_PHONE','SETTING_ORG_PHONE','-'),
	(9,'SETTING_ORG_FAX','SETTING_ORG_FAX','-'),
	(10,'SETTING_ORG_MAIL','SETTING_ORG_MAIL','-'),
	(11,'SETTING_WEB_API_FOOTERLINK','SETTING_WEB_API_FOOTERLINK','-'),
	(12,'SETTING_WEB_LOGO','SETTING_WEB_LOGO','logo.png'),
	(13,'SETTING_WEB_SKIN_CLASS','SETTING_WEB_SKIN_CLASS','skin-green-light'),
	(14,'SETTING_WEB_PRELOADER','SETTING_WEB_PRELOADER','loader.gif'),
	(15,'SETTING_WEB_VERSION','SETTING_WEB_VERSION','1.0'),
	(16,'SETTING_ORG_LINKFACEBOOK','SETTING_ORG_LINKFACEBOOK','https://www.facebook.com/Mentor-Huis-105230401189706'),
	(17,'SETTING_ORG_LINKTWITTER','SETTING_ORG_LINKTWITTER','https://twitter.com/MentorhuisID'),
	(18,'SETTING_ORG_LINKINSTAGRAM','SETTING_ORG_LINKINSTAGRAM','');

/*!40000 ALTER TABLE `_settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _userinformation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_userinformation`;

CREATE TABLE `_userinformation` (
  `UserName` varchar(50) NOT NULL,
  `Email` varchar(50) NOT NULL DEFAULT '',
  `NM_FirstName` varchar(50) DEFAULT NULL,
  `NM_LastName` varchar(50) DEFAULT NULL,
  `NM_Nickname` varchar(50) DEFAULT '',
  `NM_Gender` varchar(50) DEFAULT NULL,
  `DATE_Birth` date DEFAULT NULL,
  `NO_Phone` varchar(50) DEFAULT NULL,
  `NM_Address` text,
  `NM_Province` varchar(50) DEFAULT NULL,
  `NM_City` varchar(50) DEFAULT NULL,
  `NM_Region` varchar(50) DEFAULT NULL,
  `NM_Occupation` varchar(50) DEFAULT NULL,
  `NM_ProfileImage` varchar(250) DEFAULT NULL,
  `NUM_RateRegular` double DEFAULT NULL,
  `NUM_RateOnline` double DEFAULT NULL,
  `NUM_Experience` double DEFAULT NULL,
  `NM_Bio` text,
  `NM_MaritalStatus` varchar(50) DEFAULT NULL,
  `NM_EducationalBackground` text,
  `NM_Grade` varchar(50) DEFAULT NULL,
  `DATE_Registered` datetime DEFAULT NULL,
  `IS_EmailVerified` tinyint(1) DEFAULT NULL,
  `IS_LoginViaGoogle` tinyint(1) DEFAULT NULL,
  `IS_LoginViaFacebook` tinyint(1) DEFAULT NULL,
  `NM_GoogleOAuthID` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`UserName`),
  CONSTRAINT `FK_Userinformation_User` FOREIGN KEY (`UserName`) REFERENCES `_users` (`UserName`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_userinformation` WRITE;
/*!40000 ALTER TABLE `_userinformation` DISABLE KEYS */;

INSERT INTO `_userinformation` (`UserName`, `Email`, `NM_FirstName`, `NM_LastName`, `NM_Nickname`, `NM_Gender`, `DATE_Birth`, `NO_Phone`, `NM_Address`, `NM_Province`, `NM_City`, `NM_Region`, `NM_Occupation`, `NM_ProfileImage`, `NUM_RateRegular`, `NUM_RateOnline`, `NUM_Experience`, `NM_Bio`, `NM_MaritalStatus`, `NM_EducationalBackground`, `NM_Grade`, `DATE_Registered`, `IS_EmailVerified`, `IS_LoginViaGoogle`, `IS_LoginViaFacebook`, `NM_GoogleOAuthID`)
VALUES
	('admin','rolassimanjuntak@gmail.com','Administrator',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-17 00:00:00',NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `_userinformation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_users`;

CREATE TABLE `_users` (
  `UserName` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `RoleID` int(10) unsigned NOT NULL,
  `IsSuspend` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `LastLogin` datetime DEFAULT NULL,
  `LastLoginIP` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`UserName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_users` WRITE;
/*!40000 ALTER TABLE `_users` DISABLE KEYS */;

INSERT INTO `_users` (`UserName`, `Password`, `RoleID`, `IsSuspend`, `LastLogin`, `LastLoginIP`)
VALUES
	('admin','e10adc3949ba59abbe56e057f20f883e',999,0,'2020-10-17 19:24:26','::1');

/*!40000 ALTER TABLE `_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tmeeting
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tmeeting`;

CREATE TABLE `tmeeting` (
  `IdMeeting` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `NmJudul` varchar(200) NOT NULL DEFAULT '',
  `NmPenyelenggara` varchar(200) NOT NULL DEFAULT '',
  `NmLokasi` varchar(200) NOT NULL DEFAULT '',
  `NmKeterangan` text,
  `NmTerms` text,
  `NmVia` varchar(50) NOT NULL DEFAULT '',
  `NmURL` varchar(200) DEFAULT NULL,
  `NmSummary` text,
  `NmFile` text,
  `DateJadwal` date NOT NULL,
  `TimeFrom` varchar(5) NOT NULL DEFAULT '',
  `TimeTo` varchar(5) NOT NULL DEFAULT '',
  `IsBatal` tinyint(1) NOT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`IdMeeting`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tmeetinglogs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tmeetinglogs`;

CREATE TABLE `tmeetinglogs` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdParticipant` bigint(10) unsigned NOT NULL,
  `Timestamp` datetime NOT NULL,
  PRIMARY KEY (`Uniq`),
  KEY `FK_Log_Participant` (`IdParticipant`),
  CONSTRAINT `FK_Log_Participant` FOREIGN KEY (`IdParticipant`) REFERENCES `tmeetingparticipants` (`IdParticipant`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tmeetingparticipants
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tmeetingparticipants`;

CREATE TABLE `tmeetingparticipants` (
  `IdParticipant` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdMeeting` bigint(10) unsigned DEFAULT NULL,
  `NmPeserta` varchar(200) DEFAULT NULL,
  `NmEmail` varchar(200) DEFAULT NULL,
  `NmPhoneNo` varchar(200) DEFAULT NULL,
  `NmKeterangan` varchar(200) DEFAULT NULL,
  `IsInvitationSent` tinyint(1) NOT NULL,
  PRIMARY KEY (`IdParticipant`),
  KEY `FK_Participant_Meeting` (`IdMeeting`),
  CONSTRAINT `FK_Participant_Meeting` FOREIGN KEY (`IdMeeting`) REFERENCES `tmeeting` (`IdMeeting`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
